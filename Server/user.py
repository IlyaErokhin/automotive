from tornado.web import RequestHandler, Application, Finish
from tornado.httpserver import HTTPServer
import tornado.httputil
import tornado.process
import tornado.web
from tornado.auth import GoogleOAuth2Mixin, AuthError
from tornado import gen
import tornado.iostream
import json
from motor.motor_tornado import MotorClient
import time


class BaseHandler(RequestHandler, GoogleOAuth2Mixin):

    def initialize(self):
        self.user_db = self.settings['db']['User']['Users']
        self.car_db = self.settings['db']['Car']['Cars']
        self.share_db = self.settings['db']['Car']['QR_codes']
        self.share_db.create_index('qr_code')

    @gen.coroutine
    def prepare(self):
        access_token = self.get_argument('access_token', False)

        if not access_token:

            self.set_status(status_code=401, reason='access_token was not found')
            self.finish()
        try:

            google_user = yield self.oauth2_request(
                "https://www.googleapis.com/oauth2/v1/userinfo",
                access_token=access_token)
        except AuthError:
            self.set_status(status_code=401, reason='access_token has been expired')
            self.finish()

        user = yield self.settings['db']['User']['Users'].find_one({'_id': google_user['id']})
        if not user:
            print(user)
            google_user['_id'] = google_user.pop('id')
            google_user['cars'] = {}
            insert_result = yield self.settings['db']['User']['Users'].insert_one(google_user)
            if insert_result.acknowledged:
                user = self.settings['db']['User']['Users'].find_one({'_id': insert_result.inserted_id})
            else:
                self.set_status(stats_code=500, reason='insert document in database FAIL')
                self.finish()

        self.current_user = user


class GoogleOAuth2LoginHandler(RequestHandler, GoogleOAuth2Mixin):
    @gen.coroutine
    def get(self):
        # print(self.request)
        # print(self.request.arguments)
        # print(self.get_argument('code', 'Petux'))
        if self.get_argument('code', False):
            print('with code')
            access = yield self.get_authenticated_user(
                redirect_uri='http://localhost:8899/auth/login',
                # redirect_uri='http://ec2-18-195-187-89.eu-central-1.compute.amazonaws.com:8899/auth/login',
                code=self.get_argument('code'))
            user = yield self.oauth2_request(
                "https://www.googleapis.com/oauth2/v1/userinfo",
                access_token=access["access_token"])
            print(access["access_token"])
            print(user)
            self.write({
                "access_token": access["access_token"],
            })
        else:
            print('without code')
            yield self.authorize_redirect(
                redirect_uri='http://localhost:8899/auth/login',
                # redirect_uri='http://ec2-18-195-187-89.eu-central-1.compute.amazonaws.com:8899/auth/login',
                client_id=self.settings['google_oauth']['key'],
                scope=['profile', 'email'],
                response_type='code',
                extra_params={'approval_prompt': 'auto'})



class CarManagementHandler(BaseHandler):
    
    @gen.coroutine
    def get(self, car_id):
        car_id = car_id.upper()
        if not self.current_user['cars'].get(car_id, None):
            # self.write('You are not owner of car with nimber {0}, car with number {0} not exist'.format(car_id))
            self.set_status(status_code=400, 
                            reason='You are not owner of car with nimber {0}, car with number {0} not exist'.format(car_id))
            self.finish()
        
        car = yield self.car_db.find_one({'_id': car_id})
        # if car['owner'] == self.current_user['_id']:
        self.write(car)
        # else:
        #     self.write({
        #         'car_info': car['car_info'],
        #         'user_info': car['users'][self.current_user['_id']]
        #     })

    #add new car
    @gen.coroutine
    def put(self, car_id):
        car_id = car_id.upper()
        car = yield self.car_db.find_one({'_id': car_id})
        print(json.loads(self.request.body.decode('utf-8'))['password'])
        print(car['password'])
        if not car:
            # self.write('car with id %s not found' % car_id)
            self.set_status(status_code=400, 
                            reason='car with id %s not found' % car_id)
        elif car_id in self.current_user['cars']:
            # self.write('you already have access rights to car %s. If you want to get admin rights ask car owner.' % car_id)  
            self.set_status(status_code=400, 
                            reason='you already have access rights to car %s. If you want to get admin rights ask car owner.' % car_id)
        elif car['owner'] == self.current_user['_id']:
            # self.write({
            #     'message': 'It is already your car MAAAN.'
            # })
            self.set_status(status_code=400, 
                            reason='It is already your car')
        elif not car['owner'] and json.loads(self.request.body.decode('utf-8'))['password'] == car['password']:
            print(type(self.current_user['_id']))
            seats = ''
            mirrors = ''
            try:
                for your_car in self.current_user['cars']:
                    if your_car.get('mirrors', None):
                        mirrors = your_car['mirrors']
                    if your_car.get('seats', None):
                        seats = your_car['seats']
                    if mirrors and seats:
                        break
            except:
                pass
            
            if seats == '':
                seats = '17'
            if mirrors == '':
                mirrors = '85'
            update_result = yield self.car_db.update_one(
                {'_id': car_id}, 
                {'$set': {
                    # 'users.' + self.current_user['_id']: {}, 
                    'users.' + self.current_user['_id']: {
                        'name': self.current_user['given_name'],
                        'bluetooth': self.current_user.get('bluetooth', ''),
                        'seats': seats,
                        'music': '',
                        'power': car['power'],
                        'mirrors': mirrors,
                    },
                    'owner': self.current_user['_id']
                    }
                }
            )
            print(update_result)
            self.current_user['cars'][car_id] = {}
            yield self.user_db.update_one(
                {'_id': self.current_user['_id']}, 
                {'$set': {
                    'cars.' + car_id: {
                        'seats': seats,
                        'music': '',
                        'power': car['power'],
                        'mirrors': mirrors,
                        }
                    }
                }
            )
            if update_result.acknowledged:
                car = yield self.car_db.find_one({'_id': car_id})
                self.write(json.dumps(car))
            else:
                self.set_status(status_code=500, 
                                reason='Problem with updating db')
        else:
            self.set_status(status_code=500, 
                            reason='some shit')
    @gen.coroutine
    def post(self, car_id):
        car_id = car_id.upper()
        car = yield self.car_db.find_one({'_id': car_id})
        if not car:
            # self.write('Car with id %s is not exist')
            self.set_status(status_code=400, 
                            reason='Car with id %s is not exist' % car_id)
        # elif self.current_user['_id'] != car['owner']:
        #     self.write('Current user doesnt have rights to change this car')
        elif self.current_user['_id'] in car['users']:
            settings = json.loads(self.request.body.decode('utf-8'))
            #TODO: some fields must be deleted
            # print('CAR BEFORE')
            # print(car)

            if settings:
                self.car_db.update_one({
                    '_id': car_id
                }, {
                    '$set': settings
                })
            # n_car = yield self.car_db.find_one({'_id': car_id})
            # print('CAR AFTER')
            # print(n_car)
        


class GetAllCarsHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        coursor = self.car_db.find({"_id" : { "$in" : list(self.current_user['cars'].keys())}})
        response = [car for car in (yield coursor.to_list(None))]
        self.write(json.dumps(response))


class ShareGiveCareHandler(BaseHandler):
    @gen.coroutine
    def get(self, car_id):
        car_id = car_id.upper()
        car = yield self.car_db.find_one({'_id': car_id})
        if not car:
            # self.write("Car with id %s doesn't exist", car_id)
            self.set_status(status_code=400, 
                            reason="Car with id %s doesn't exist" % car_id)
            self.finish()
        if car['owner'] != self.current_user['_id']:
            # self.write("You don't have rights to share this car")
            self.set_status(status_code=400, 
                            reason="You don't have rights to share this car")
            self.finish()
        qr_code = '$'.join([
            self.current_user['_id'], 
            str(round(time.time() * 100000)), 
            car['_id']
        ])

        # yield self.car_db.update_one({'_id': car_id}, {'$set': {'qr_code': qr_code}})
        yield self.share_db.update_one({'_id': car_id}, {'$set': {'qr_code': qr_code}}, upsert=True)

        self.write(json.dumps({
            'qr_code': qr_code
        }))
        self.car_id = car_id

    @gen.coroutine
    def on_finish(self):
        yield gen.sleep(5 * 60) #delete qr-code after 10 minutes
        # self.car_db.update_one({'_id': self.car_id}, {'$set': {'qr_code': ''}})
        yield self.share_db.delete_one({'_id': self.car_id})
        print("qr_code for %s was deleted" % self.car_id)


class ShareGetCareHandler(BaseHandler):

    @gen.coroutine
    def post(self):
        qr_code = json.loads(self.request.body.decode('utf-8'))['qr_code']
        if not qr_code:
            self.set_status(status_code=400, 
                            reason='qr_code was not sended, please send in body {"qr_code": "qr code value from GET /car/([0-9]+)/share response"}')
            # self.write('qr_code was not sended, please send in body {"qr_code": "qr code value from GET /car/([0-9]+)/share response"}')
            self.finish()


        ####################
        share_item = yield self.share_db.find_one({'qr_code': qr_code})
        car = yield self.car_db.find_one({'_id': share_item['_id']})
        ####################

        # car = yield self.car_db.find_one({'qr_code': qr_code})
        if not car:
            # self.write('qr_code was not found')
            self.set_status(status_code=400, 
                            reason='qr_code was not found')
            self.finish()
        seats = ''
        mirrors = ''
        try:
            for your_car in self.current_user['cars']:
                if your_car.get('mirrors', None):
                    mirrors = your_car['mirrors']
                if your_car.get('seats', None):
                    seats = your_car['seats']
                if mirrors and seats:
                    break
        except:
            pass
        
        if seats == '':
            seats = '17'
        if mirrors == '':
            mirrors = '85'
        yield self.car_db.update_one({
            '_id': car['_id']
        }, {
            # '$push': {
            #     'users': self.current_user['_id']
            # },
            '$set': {
                # 'qr_code': '',
                'users.' + self.current_user['_id']: {
                        'seats': seats,
                        'music': '',
                        'power': car['power'],
                        'mirrors': mirrors,
                }
            }
        })
        print('ssssss')
        yield self.user_db.update_one(
            {'_id': self.current_user['_id']}, 
            {'$set': {
                'cars.' + car['_id']: {
                        'name': self.current_user['given_name'],
                        'bluetooth': self.current_user.get('bluetooth', ''),
                        'seats': seats,
                        'music': '',
                        'power': car['power'],
                        'mirrors': mirrors,
                    }
                }
            }
        )
        yield self.share_db.delete_one({'_id': car['_id']})
        self.write("Car {0} was added successfully to your car list".format(car['_id']))


class SaveBlueToothHandler(BaseHandler):

    @gen.coroutine
    def post(self):
        bluetooth = ''
        if self.request.body:
            bluetooth = json.loads(self.request.body.decode('utf-8'))['bluetooth']
        if bluetooth:
            yield self.user_db.update_one({
                '_id': self.current_user['_id']
            }, {
                '$set': {
                    'bluetooth': bluetooth
                }
            })
            yield self.car_db.update_many({
                'users.' + self.current_user['_id']: {'$exists': True}
            }, {
                '$set':{
                    'users.' + self.current_user['_id'] + '.bluetooth': bluetooth
                }
            })
            self.write('bluetooth was added')
        else:
            self.set_status(status_code=400, 
                            reason='bluetooth faild')
            # self.write('bluetooth faild')


settings = {
    'google_oauth': {
        'key': '299478560599-ro74rg0mmrbnr4t0kuqsivu5c3ssn6a6.apps.googleusercontent.com',
        'secret': '596gE6vy3tPvB-cMY1fc6bcO',
    },
}

urls = [
    (r'/auth/login', GoogleOAuth2LoginHandler),
    # (r'/car/([0-9]+)', UserCar),
    # (r'/car/([0-9]+)/user/([0-9]+)', UserManagementHandler),
    (r'/car/getaccess', ShareGetCareHandler),
    (r'/bluetooth', SaveBlueToothHandler), 
    (r'/car', GetAllCarsHandler), # get=all cars, 
    (r'/car/([0-9a-zA-Z]+)/share', ShareGiveCareHandler), # get= get qr-code for sharing
    (r'/car/([0-9a-zA-Z]+)', CarManagementHandler),# get=car info, put=add new car
     #post = get access rights to car with qr code
    
    
   
    
    # (r'/user/([0-9]+)/add', User),
    # (r'/', User),
    # (r'/user/([0-9]+)/car/([0-9]+)/', UserCar),
    # (r'/car/()', UsersInCarList),
    # (r'/', MainHandler),
]

    # _OAUTH_AUTHORIZE_URL = "https://accounts.google.com/o/oauth2/v2/auth"
    # _OAUTH_ACCESS_TOKEN_URL = "https://www.googleapis.com/oauth2/v4/token"
    # _OAUTH_USERINFO_URL = "https://www.googleapis.com/oauth2/v1/userinfo"


if __name__ == '__main__':
    port = 8899

    application = Application(urls, **settings)
    httpServer = HTTPServer(application)
    httpServer.bind(port)
    httpServer.start(0)
    application.settings['db'] = MotorClient('localhost', 27017)
    tornado.ioloop.IOLoop.current().start()
    