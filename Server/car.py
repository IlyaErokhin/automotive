from tornado.web import RequestHandler, Application, Finish
from tornado.httpserver import HTTPServer
import tornado.httputil
import tornado.process
import tornado.web
from tornado import gen
import tornado.iostream
import json
from motor.motor_tornado import MotorClient


class BaseHandler(RequestHandler):

    def initialize(self):
        self.car_db = self.settings['db']['Car']['Cars']
        self.user_db = self.settings['db']['User']['Users']


class UploadCarToDBHandler(BaseHandler):

    @gen.coroutine
    def put(self):
        car = json.loads(self.request.body.decode('utf-8'))
        car['_id'] = car['_id'].upper()
        yield self.car_db.update_one(
            {'_id': car['_id']}, 
            {'$set': car}, 
            upsert=True
        )
        self.write("Car was added or updated")

class GetCarsListHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        response = []
        coursor = self.car_db.find({})
        response = [car for car in (yield coursor.to_list(None))]
        self.write(json.dumps(response))

class SendCarDataHandler(BaseHandler):

    @gen.coroutine
    def prepare(self):
        data = None
        try:
            data = json.loads(self.request.body.decode('utf-8'))
        except json.decoder.JSONDecodeError:
            self.set_status(status_code=400, reason='wrong body')
            self.finish()
        if not '_id' in data:
            # self.write("login was not given")
            self.set_status(status_code=401, reason="'_id' was not given")
            self.finish()
        elif not 'password' in data:
            # self.write("password was not given")
            self.set_status(status_code=401, reason='password was not given')
            self.finish()
        data['_id'] = data['_id'].upper()
        self.car = yield self.car_db.find_one({'_id': data['_id']})
        if not self.car:
            self.set_status(status_code=401, reason="car with id %s doesn't exist" % data['_id'])
            # self.write("car with id %s doesn't exist" % data['login'])
            self.finish()
        self.data = data

    # @gen.coroutine
    # def get(self):
    #     # data = json.loads(self.request.body.decode('utf-8'))
    #     # if not 'login' in data:
    #     #     self.write("login was not given")
    #     #     self.finish()
    #     # elif 'password' in data:
    #     #     self.write("password was not given")
    #     #     self.finish()
    #     # car = yield self.car_db.find_one({'_id': data['login']})
    #     # if not car:
    #     #     self.write("car with id %s doesn't exist" % data['login'])
    #     #     self.finish()
        
    #     if self.car['password'] == self.data['password']:
    #         self.write(self.car)
    #     else:
    #         self.write('incorrect password')

    @gen.coroutine
    def post(self):

        new_car = json.loads(self.request.body.decode('utf-8'))
        if not new_car:
            self.write("settings is empty")
            self.finish()

        car = self.car_db.update_one({
            '_id': self.car['_id']
        }, {
            '$set': new_car
        })

        self.write('settings of car were updated successfully')


class GetCarInfoHandler(BaseHandler):

    @gen.coroutine
    def get(self, car_id):
        car_id = car_id.upper()
        password = self.get_argument('password', False)
        if not password:
            self.set_status(status_code=401, reason='password was not found')
            self.finish()
        
        car = yield self.car_db.find_one({'_id': car_id})
        if car['password'] == password:
            self.write(car)
        else:
            self.set_status(status_code=401, reason='lagin/password pair is incorrect')


class ChangeUserSettings(BaseHandler):
    
    # @gen.coroutine
    # def prepare(self):
    #     data = None
    #     try:
    #         data = json.loads(self.request.body.decode('utf-8'))
    #     except json.decoder.JSONDecodeError:
    #         self.set_status(status_code=400, reason='wrong body')
    #         self.finish()
    #     if not '_id' in data:
    #         # self.write("login was not given")
    #         self.set_status(status_code=401, reason="'_id' was not given")
    #         self.finish()
    #     elif not 'password' in data:
    #         # self.write("password was not given")
    #         self.set_status(status_code=401, reason='password was not given')
    #         self.finish()
    #     data['_id'] = data['_id'].upper()
    #     self.car = yield self.car_db.find_one({'_id': data['_id']})
    #     if not self.car:
    #         self.set_status(status_code=401, reason="car with id %s doesn't exist" % data['_id'])
    #         # self.write("car with id %s doesn't exist" % data['login'])
    #         self.finish()
    #     self.data = data

    @gen.coroutine
    def post(self, car_id, user_id):
        try:
            print(self.request.body.decode('utf-8'))
            user_settings = json.loads(self.request.body.decode('utf-8'))
            # print(user_settings)
        except json.decoder.JSONDecodeError:
            self.set_status(status_code=400, reason='wrong body')
            self.finish()
        # print(user_settings)
        if len(list(user_settings.keys())) > 1:
            self.set_status(status_code=400, reason='wrong body')
            self.finish()
        # user_id = list(user_settings.keys())[0]
        car_id = car_id.upper()
        car = yield self.car_db.find_one({'_id': car_id})
        try:
            if user_settings.get('power', car['power']) > car['power']:
                user_settings['power'] = car['power']
        except:
            user_settings['power'] = car['power']

        yield self.user_db.update_one(
            {'_id': user_id},
            {'$set': {
                'cars.' + car_id: user_settings
            }
            })
        yield self.car_db.update_one(
            {'_id': car_id},
            {'$set': {
                'users.' + user_id: user_settings
            }})
        self.write('Success nahui')
        

settings = {}


urls = [
    (r'/upload/car', UploadCarToDBHandler),
    (r'/car', GetCarsListHandler),
    (r'/([0-9a-zA-Z]+)/change/user/([0-9]+)', ChangeUserSettings),
    (r'/', SendCarDataHandler),
    (r'/([0-9a-zA-Z]+)', GetCarInfoHandler),
    

]

if __name__ == '__main__':
    port = 8877
    application = Application(urls, **settings)
    httpServer = HTTPServer(application)
    httpServer.bind(port)
    httpServer.start(0)
    application.settings['db'] = MotorClient('localhost', 27017)
    tornado.ioloop.IOLoop.current().start()
    