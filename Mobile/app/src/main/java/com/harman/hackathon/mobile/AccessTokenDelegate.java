package com.harman.hackathon.mobile;

public interface AccessTokenDelegate {
    void setAccessToken(String accessToken);
    String getAccessToken();
}
