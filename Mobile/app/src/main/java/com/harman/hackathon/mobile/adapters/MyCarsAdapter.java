package com.harman.hackathon.mobile.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harman.hackathon.mobile.AutoActivity;
import com.harman.hackathon.mobile.R;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;

import java.util.ArrayList;

public class MyCarsAdapter extends RecyclerView.Adapter<MyCarsAdapter.CarsViewHolder>{

    private ArrayList<Car> mCars;
    private Activity mActivity;

    public MyCarsAdapter(ArrayList<Car> cars, Activity activity) {
        mCars = cars;
        mActivity = activity;
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car, parent, false);
        CarsViewHolder holder = new CarsViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final CarsViewHolder holder, int position) {
        holder.mTvName.setText(mCars.get(position).getName());
        holder.mTvOpened.setText(mCars.get(position).isOpened() ? "true" : "false");
        holder.mTvStarted.setText(mCars.get(position).isStarted() ? "true" : "false");

        holder.mCvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBManager.getInstance().put(DBKeys.CURRENT_CAR, mCars.get(holder.getAdapterPosition()));
                mActivity.startActivity(new Intent(mActivity,AutoActivity.class));
                mActivity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCars.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CarsViewHolder extends RecyclerView.ViewHolder {
        TextView mTvName;
        TextView mTvStarted;
        TextView mTvOpened;
        CardView mCvCard;

        CarsViewHolder(View itemView) {
            super(itemView);

            mTvName = itemView.findViewById(R.id.tvName);
            mTvStarted = itemView.findViewById(R.id.tvIsStarted);
            mTvOpened = itemView.findViewById(R.id.tvIsOpened);
            mCvCard = itemView.findViewById(R.id.cvCard);
        }
    }
}
