package com.harman.hackathon.mobile.api;

import com.harman.hackathon.mobile.data.Bluetooth;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.data.Password;
import com.harman.hackathon.mobile.data.QrCode;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AutoApi {

    @PUT("/car/{car_id}")
    Call<Car> addCar(
            @Path("car_id") String carID,
            @Query("access_token") String accessToken,
            @Body Password password
    );

    @GET("/car")
    Call<ArrayList<Car>> getCarList(
            @Query("access_token") String accessToken
    );

    @GET("/car/{car_id}/share")
    Call<QrCode> getQrCode(
            @Path("car_id") String carID,
            @Query("access_token") String accessToken
    );

    @POST("/car/getaccess")
    Call<ResponseBody> addCarByQRCode(
            @Query("access_token") String accessToken,
            @Body QrCode qrCode
    );

    @POST("/car/{car_id}")
    Call<ResponseBody> updateCar(
            @Path("car_id") String carID,
            @Query("access_token") String accessToken,
            @Body Car car
    );

    @POST("/bluetooth")
    Call<ResponseBody> updateBluetooth(
            @Query("access_token") String accessToken,
            @Body Bluetooth bluetooth
    );
}
