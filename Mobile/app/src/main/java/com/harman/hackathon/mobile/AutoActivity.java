package com.harman.hackathon.mobile;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.harman.hackathon.mobile.data.Bluetooth;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.AudioPlayer;
import com.harman.hackathon.mobile.utils.ContextUtil;

import java.util.ArrayList;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoActivity extends Activity
        implements TextToSpeech.OnInitListener {
    private static final String TAG = "AutoActivity";

    private ImageView mBtnEngine;
    private ImageView mBtnOpen;
    private ImageView mBtnBle;
    private ImageView mBtnQR;
    private ImageView mBtnCar;
    private ImageView mBtnService;

    private Activity mActivity;
    private TextToSpeech mTTS;

    private String mName = "user";

    private static final int REQUEST_ENABLE_BT = 2343;

    private static final int REQUEST_SERVICE = 3435;

    private BluetoothAdapter mBluetoothAdapter;

    private Car mCurrentCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto);

        mActivity = this;

        mTTS = new TextToSpeech(this, this);

        mBtnEngine = findViewById(R.id.btnEngine);
        mBtnOpen = findViewById(R.id.btnOpen);
        mBtnBle = findViewById(R.id.btnBle);
        mBtnQR = findViewById(R.id.btnQr);
        mBtnCar = findViewById(R.id.btnCar);
        mBtnService = findViewById(R.id.btnService);

        if(DBManager.getInstance().exists(DBKeys.CURRENT_CAR)) {
            mCurrentCar = (Car)DBManager.getInstance().get(DBKeys.CURRENT_CAR, Car.class);
        } else {
            startActivity(new Intent(this, MyCarsActivity.class));
            finish();
        }

        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter != null) {
            mBtnBle.setImageDrawable(mBluetoothAdapter.isEnabled()
                    ? ContextCompat.getDrawable(this, R.drawable.ble)
                    : ContextCompat.getDrawable(this, R.drawable.ble_off));
        }

        ((TextView)findViewById(R.id.tvName)).setText(mCurrentCar.getName());

        mBtnEngine.setImageDrawable(ContextCompat.getDrawable(this,
                mCurrentCar.isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));

        mBtnOpen.setImageDrawable(ContextCompat.getDrawable(this,
                mCurrentCar.isOpened() ? R.drawable.unlocked : R.drawable.locked));

        mBtnEngine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mCurrentCar.isStarted()) new AudioPlayer().play(R.raw.engine);
                mCurrentCar.setStarted(!mCurrentCar.isStarted());
//                mBtnEngine.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
//                        mCurrentCar.isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));
                updateCar();
            }
        });

        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new AudioPlayer().play(R.raw.open);
                mCurrentCar.setOpened(!mCurrentCar.isOpened());
//                mBtnOpen.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
//                        mCurrentCar.isOpened() ? R.drawable.unlocked : R.drawable.locked));
                updateCar();
            }
        });

        mBtnBle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mActivity.startActivity(new Intent(mActivity,BleActivity.class));

                if (mBluetoothAdapter != null) {
                    if(!mBluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    } else {
                        mBluetoothAdapter.disable();
                        mBtnBle.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ble_off));
                    }
                }

                Bluetooth bluetooth = new Bluetooth(android.provider.Settings.Secure.getString(mActivity.getContentResolver(), "bluetooth_address"));
                updateBluetooth(bluetooth);
            }
        });

        mBtnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.startActivity(new Intent(mActivity,QRActivity.class));
            }
        });

        mBtnCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.startActivity(new Intent(mActivity,MyCarsActivity.class));
                mActivity.finish();
            }
        });

        mBtnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.startActivityForResult(new Intent(mActivity,ServiceActivity.class), REQUEST_SERVICE);
            }
        });

        updateCars();
    }

    public static Car getCarForTesting() {
        return new Car();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            Locale locale = new Locale("en");

            int result = mTTS.setLanguage(locale);
            //int result = mTTS.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Извините, этот язык не поддерживается");
            } else {
                //mTTS.speak("Hello" + mName + "! Where are we going?", TextToSpeech.QUEUE_FLUSH, null);
            }

        } else {
            Log.e("TTS", "Ошибка!");
        }
    }

    private void updateCar() {
        DBManager.getInstance().put(DBKeys.CURRENT_CAR, mCurrentCar);
        ContextUtil.getAutoApi().updateCar(mCurrentCar.getCarID(),
                ContextUtil.getAccessTokenDelegate().getAccessToken(),
                mCurrentCar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 401) {
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void updateBluetooth(Bluetooth bluetooth) {
        ContextUtil.getAutoApi().updateBluetooth(ContextUtil.getAccessTokenDelegate().getAccessToken(), bluetooth).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 401) {
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void updateCars() {
        ContextUtil.getAutoApi().getCarList(ContextUtil.getAccessTokenDelegate().getAccessToken())
                .enqueue(new Callback<ArrayList<Car>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                        if (response.isSuccessful()) {
                            ArrayList<Car> mCars = response.body();
                            DBManager.getInstance().put(DBKeys.CARS_LIST, mCars);

                            if (mCars != null) {
                                for (Car car : mCars) {
                                    if (mCurrentCar.getCarID().contentEquals(car.getCarID())) {
                                        if(mCurrentCar.isOpened() != car.isOpened() || mCurrentCar.isStarted() != car.isStarted()) {
                                            if (mCurrentCar.isStarted() != car.isStarted()) {
                                                mBtnEngine.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                                        car.isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));
                                            }
                                            if (mCurrentCar.isOpened() != car.isOpened()) {
                                                mBtnOpen.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                                        car.isOpened() ? R.drawable.unlocked : R.drawable.locked));
                                            }
                                            mCurrentCar = car;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        updateCars();
                    }

                    @Override
                    public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                        //Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        updateCars();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                mBtnBle.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ble));
            }
        }

        if (requestCode == REQUEST_SERVICE){
            if (requestCode == Activity.RESULT_OK){
                String name = data.getStringExtra("name");
                String date = data.getStringExtra("date");
                String time = data.getStringExtra("time");

                String day_for_speak = getResources().getStringArray(R.array.days_for_speak)[Integer.parseInt(date.substring(0,1)) - 1];
                String date_for_speak = day_for_speak + getResources().getStringArray(R.array.months_for_speak)[Integer.parseInt(date.substring(3,4)) - 1];
                String time_for_speak = getResources().getStringArray(R.array.hours_for_speak)[Integer.parseInt(time.substring(0,1))];

                String textToSpeak = name + ", вы записаны на " + time_for_speak +  date_for_speak;
                Toast.makeText(AutoActivity.this, "You are recorded." + date + time, Toast.LENGTH_LONG).show();
                mTTS.speak("", TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }
}
