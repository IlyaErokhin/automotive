package com.harman.hackathon.mobile.utils;

import android.content.Context;

import com.harman.hackathon.mobile.AccessTokenDelegate;
import com.harman.hackathon.mobile.AutoApiDelegate;
import com.harman.hackathon.mobile.AutoApplication;
import com.harman.hackathon.mobile.api.AutoApi;

public class ContextUtil {

    private static Context appContext;

    public static void setAppContext(Context context) {
        if (appContext == null) {
            appContext = context;
        }
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static AccessTokenDelegate getAccessTokenDelegate() {
        return (AccessTokenDelegate) appContext;
    }

    public static AutoApi getAutoApi() {
        return ((AutoApiDelegate) getAppContext()).getApi();
    }
}
