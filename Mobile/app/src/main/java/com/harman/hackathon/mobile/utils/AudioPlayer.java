package com.harman.hackathon.mobile.utils;

import android.media.MediaPlayer;

public class AudioPlayer {

    private MediaPlayer mMediaPlayer;

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // mothod for raw folder (R.raw.fileName)
    public void play(int rid){
        stop();

        mMediaPlayer = MediaPlayer.create(ContextUtil.getAppContext(), rid);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });

        mMediaPlayer.start();
    }
}
