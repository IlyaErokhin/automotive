package com.harman.hackathon.mobile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.data.QrCode;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.ContextUtil;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class QRDecoderActivity extends Activity implements QRCodeReaderView.OnQRCodeReadListener {
    private static final String TAG = "QRDecoderActivity";

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1234;

    private QRCodeReaderView qrCodeReaderView;
    private boolean qrDecoderFlag = false;

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MyCarsActivity.class));
        finish();
    }

    private Activity mActivity;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Intent intent = new Intent(mActivity, QRDecoderActivity.class);
        mActivity.startActivity(intent);
        mActivity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrdecoder);

        mActivity = this;

        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CAMERA)) {


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }

        }

        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        //qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed in View
    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        if(qrDecoderFlag) return;
        qrDecoderFlag = true;
        //Toast.makeText(getApplicationContext(), "Code: " + text, Toast.LENGTH_SHORT).show();
        QrCode qrCode = new QrCode();
        qrCode.setQrCode(text);

        ContextUtil.getAutoApi().addCarByQRCode(ContextUtil.getAccessTokenDelegate().getAccessToken(),
                qrCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    ContextUtil.getAutoApi().getCarList(ContextUtil.getAccessTokenDelegate().getAccessToken())
                            .enqueue(new Callback<ArrayList<Car>>() {
                                @Override
                                public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                                    if (response.isSuccessful()) {
                                        ArrayList<Car> cars = response.body();
                                        DBManager.getInstance().put(DBKeys.CARS_LIST, cars);

                                        Intent intent = new Intent(mActivity, MyCarsActivity.class);
                                        mActivity.startActivity(intent);
                                        mActivity.finish();
                                    } else {
                                        //Toast.makeText(getApplicationContext(), "response.isSuccessful() == false: " + response.message(), Toast.LENGTH_SHORT).show();
                                        if (response.code() == 401) {
                                            if (response.code() == 401) {
                                                Intent intent = new Intent(mActivity, LoginActivity.class);
                                                mActivity.startActivity(intent);
                                                mActivity.finish();
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                                    //Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mActivity.finish();
            }
        });
    }
}
