package com.harman.hackathon.mobile.db;

public final class DBKeys {
    public static final String TOKEN = "db.keys.token";
    public static final String CURRENT_CAR = "db.keys.current_car";
    public static final String CARS_LIST = "db.keys.cars_list";
}
