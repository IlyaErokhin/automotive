package com.harman.hackathon.mobile;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.ContextUtil;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity
        implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "LoginActivity";

    private static final int RC_AUTH_CODE = 1234;

    private final static String G_PLUS_SCOPE =
            "oauth2:https://www.googleapis.com/auth/plus.me";
    private final static String USERINFO_SCOPE =
            "https://www.googleapis.com/auth/userinfo.profile";
    private final static String EMAIL_SCOPE =
            "https://www.googleapis.com/auth/userinfo.email";
    private final static String SCOPES = G_PLUS_SCOPE + " " + USERINFO_SCOPE + " " + EMAIL_SCOPE;

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = this;

        findViewById(R.id.tvSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity,SignUpActivity.class));
            }
        });

        findViewById(R.id.activity_button_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"},
                        false, null, null, null, null);
                startActivityForResult(intent, RC_AUTH_CODE);
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Toast.makeText(getApplicationContext(), "onConnectionFailed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_AUTH_CODE && resultCode == RESULT_OK) {
            final String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            AsyncTask<Void, Void, String> getToken = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    try {
                        String token = GoogleAuthUtil.getToken(LoginActivity.this, accountName,
                                SCOPES);

                        return token;

                    } catch (UserRecoverableAuthException userAuthEx) {
                        startActivityForResult(userAuthEx.getIntent(), RC_AUTH_CODE);
                    }  catch (IOException ioEx) {
                        Log.d(TAG, "IOException");
                    }  catch (GoogleAuthException fatalAuthEx)  {
                        Log.d(TAG, "Fatal Authorization Exception" + fatalAuthEx.getLocalizedMessage());
                    }
                    return "null";
                }

                @Override
                protected void onPostExecute(String token) {
                    //Toast.makeText(getApplicationContext(), "Token: " + token, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, token);
                    if (token != null) {
                        DBManager.getInstance().put(DBKeys.TOKEN, token);
                        ContextUtil.getAccessTokenDelegate().setAccessToken(token);
                    }

                    ContextUtil.getAutoApi().getCarList(ContextUtil.getAccessTokenDelegate().getAccessToken())
                            .enqueue(new Callback<ArrayList<Car>>() {
                                @Override
                                public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                                    if (response.isSuccessful()) {
                                        ArrayList<Car> cars = response.body();
                                        DBManager.getInstance().put(DBKeys.CARS_LIST, cars);

                                        if (cars != null && cars.size() > 0) {
                                            new Handler() {
                                                @Override
                                                public void handleMessage(Message msg) {
                                                    if (DBManager.getInstance().exists(DBKeys.CURRENT_CAR)) {
                                                        Intent intent = new Intent(activity, AutoActivity.class);
                                                        activity.startActivity(intent);
                                                        activity.finish();
                                                    } else {
                                                        Intent intent = new Intent(activity, MyCarsActivity.class);
                                                        activity.startActivity(intent);
                                                        activity.finish();
                                                    }
                                                }
                                            }.sendEmptyMessageDelayed(0, 2500);
                                        } else {
                                            new Handler() {
                                                @Override
                                                public void handleMessage(Message msg) {
                                                    Intent intent = new Intent(activity, MyCarsActivity.class);
                                                    activity.startActivity(intent);
                                                    activity.finish();
                                                }
                                            }.sendEmptyMessageDelayed(0, 2500);
                                        }
                                    } else {
                                        //Toast.makeText(getApplicationContext(), "response.isSuccessful() == false: " + response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                                    //Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }

            };
            getToken.execute(null, null, null);
        }
    }
}
