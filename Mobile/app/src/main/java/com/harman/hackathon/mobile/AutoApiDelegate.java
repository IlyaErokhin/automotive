package com.harman.hackathon.mobile;

import com.harman.hackathon.mobile.api.AutoApi;

public interface AutoApiDelegate {
    AutoApi getApi();
}
