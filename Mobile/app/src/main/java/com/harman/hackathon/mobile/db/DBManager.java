package com.harman.hackathon.mobile.db;

import com.harman.hackathon.mobile.utils.ContextUtil;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

public class DBManager {

    private static DBManager instance;

    private DB snappydb;

    private DBManager() {
        open();
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void put(String key, Object value) {
        open();
        try {
            snappydb.put(key, value);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public Object get(String key, Class className) {
        open();
        try {
            return snappydb.getObject(key, className);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean exists(String key) {
        open();
        try {
            return snappydb.exists(key);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void open() {
        try {
            if (snappydb == null || !snappydb.isOpen()) {
                snappydb = DBFactory.open(ContextUtil.getAppContext());
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        snappydb.close();
        snappydb.destroy();
    }
}
