package com.harman.hackathon.mobile;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.harman.hackathon.mobile.api.AutoApi;
import com.harman.hackathon.mobile.utils.ContextUtil;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AutoApplication extends Application
        implements AccessTokenDelegate, AutoApiDelegate {

    private static String mAccessToken = null;

    private static AutoApi autoApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        ContextUtil.setAppContext(this);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://ec2-18-195-187-89.eu-central-1.compute.amazonaws.com:8899/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        autoApi = retrofit.create(AutoApi.class);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    @Override
    public String getAccessToken() {
        return mAccessToken;
    }

    public AutoApi getApi() {
        return autoApi;
    }
}
