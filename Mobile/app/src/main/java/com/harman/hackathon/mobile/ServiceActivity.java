package com.harman.hackathon.mobile;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ServiceActivity extends Activity {

    private EditText etName;
    private EditText etPhone;
    private EditText etEmail;
    private EditText etTime;
    private EditText etDate;
    private Button btnSend;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etTime = (EditText) findViewById(R.id.etTime);
        etDate = (EditText) findViewById(R.id.etDate);

        btnSend = (Button) findViewById(R.id.btnSend);

        intent = getIntent();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.getText()== null) {
                    Toast.makeText(ServiceActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
                } else if(etTime.getText() == null){
                    Toast.makeText(ServiceActivity.this, "Please enter time", Toast.LENGTH_SHORT).show();
                } else if (etDate.getText() == null){
                    Toast.makeText(ServiceActivity.this, "Please enter date", Toast.LENGTH_SHORT).show();
                }else{
                    intent.putExtra("name", etName.getText().toString());
                    intent.putExtra("time", etTime.getText().toString());
                    intent.putExtra("date", etDate.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });


    }
}
