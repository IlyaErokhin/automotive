package com.harman.hackathon.mobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.harman.hackathon.mobile.adapters.MyCarsAdapter;
import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.data.Password;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.ContextUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCarsActivity extends Activity {
    private static final String TAG = "MyCarsActivity";

    private ImageView mBtnAddQR;
    private ImageView mBtnAdd;
    private MaterialDialog.Builder mDialogAddCar;
    private MyCarsAdapter mAdapter;

    private Activity mActivity;

    private ArrayList<Car> mCars;

    private RecyclerView rv;

    @Override
    protected void onResume() {
        super.onResume();
        ContextUtil.getAutoApi().getCarList(ContextUtil.getAccessTokenDelegate().getAccessToken())
                .enqueue(new Callback<ArrayList<Car>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                        if (response.isSuccessful()) {
                            mCars = response.body();
                            DBManager.getInstance().put(DBKeys.CARS_LIST, mCars);
                            mAdapter = new MyCarsAdapter(mCars, mActivity);
                            rv.setAdapter(mAdapter);
                        } else {
                            //Toast.makeText(getApplicationContext(), "response.isSuccessful() == false: " + response.message(), Toast.LENGTH_SHORT).show();
                            if (response.code() == 401) {
                                Intent intent = new Intent(mActivity, LoginActivity.class);
                                mActivity.startActivity(intent);
                                mActivity.finish();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                        //Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cars);

        mActivity = this;

        initDialogAddCar();

        mBtnAddQR = findViewById(R.id.btnAddQR);
        mBtnAdd = findViewById(R.id.btnAdd);

        rv = findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        if (DBManager.getInstance().exists(DBKeys.CARS_LIST)) {
            mCars = (ArrayList<Car>) DBManager.getInstance().get(DBKeys.CARS_LIST, ArrayList.class);
            mAdapter = new MyCarsAdapter(mCars, this);
            rv.setAdapter(mAdapter);
        } else {
            mDialogAddCar.show();
        }

        mBtnAddQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.startActivity(new Intent(mActivity,QRDecoderActivity.class));
                mActivity.finish();
            }
        });

        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogAddCar.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (DBManager.getInstance().exists(DBKeys.CURRENT_CAR)) {
            startActivity(new Intent(this, AutoActivity.class));
        }
        finish();
    }

    private void initDialogAddCar() {
        mDialogAddCar = new MaterialDialog.Builder(mActivity)
                .contentColor(ContextCompat.getColor(mActivity,R.color.colorMainDark))
                .customView(R.layout.dialog_add_car, false)
                .title(R.string.add_dialog_title)
                .titleColor(ContextCompat.getColor(mActivity,R.color.colorMain))
                .positiveText(R.string.add_dialog_ok)
                .positiveColor(ContextCompat.getColor(mActivity,R.color.colorMain))
                .negativeText(R.string.add_dialog_cancle)
                .negativeColor(ContextCompat.getColor(mActivity,R.color.colorMainAccent))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {
                        String code = ((EditText)dialog.findViewById(R.id.etCode)).getText().toString();
                        String vim = ((EditText)dialog.findViewById(R.id.etVim)).getText().toString();

                        if(code.length() == 0 || vim.length() == 0) dialog.dismiss();

                        //Toast.makeText(getApplicationContext(), "Code: " + code, Toast.LENGTH_SHORT).show();

                        Password password = new Password(code);

                        ContextUtil.getAutoApi().addCar(vim,
                                ContextUtil.getAccessTokenDelegate().getAccessToken(),
                                password).enqueue(new Callback<Car>() {
                            @Override
                            public void onResponse(Call<Car> call, Response<Car> response) {
                                if (response.isSuccessful()) {
                                    Car car = response.body();
                                    if (DBManager.getInstance().exists(DBKeys.CARS_LIST)) {
                                        mCars = (ArrayList<Car>) DBManager.getInstance().get(DBKeys.CARS_LIST, ArrayList.class);
                                        mCars.add(car);
                                        DBManager.getInstance().put(DBKeys.CARS_LIST, mCars);
                                        mAdapter = new MyCarsAdapter(mCars, mActivity);
                                        rv.setAdapter(mAdapter);
                                    }
                                    //Toast.makeText(getApplicationContext(), "onResponse: " + car.getName(), Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                } else {
                                    if (response.code() == 401) {
                                        Intent intent = new Intent(mActivity, LoginActivity.class);
                                        mActivity.startActivity(intent);
                                        mActivity.finish();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Car> call, Throwable t) {
                                String response = t.getMessage();
                                Log.d(TAG, response);
                                //Toast.makeText(getApplicationContext(), "onFailure: " + response, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                });


    }
}
