package com.harman.hackathon.mobile.data;


public class QrCode {
    private String qr_code;

    public String getQrCode() {
        return qr_code;
    }

    public void setQrCode(String qrCode) {
        qr_code = qrCode;
    }
}
