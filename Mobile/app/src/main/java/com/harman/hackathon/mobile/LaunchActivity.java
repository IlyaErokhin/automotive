package com.harman.hackathon.mobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.AudioPlayer;
import com.harman.hackathon.mobile.utils.ContextUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaunchActivity extends Activity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        mActivity = this;

        new AudioPlayer().play(R.raw.engine);

        if (DBManager.getInstance().exists(DBKeys.TOKEN)) {
            String token = (String)DBManager.getInstance().get(DBKeys.TOKEN, String.class);
            ContextUtil.getAccessTokenDelegate().setAccessToken(token);
            //Toast.makeText(getApplicationContext(), "Token: " + token, Toast.LENGTH_SHORT).show();

            ContextUtil.getAutoApi().getCarList(ContextUtil.getAccessTokenDelegate().getAccessToken())
                    .enqueue(new Callback<ArrayList<Car>>() {
                @Override
                public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                    if (response.isSuccessful()) {
                        ArrayList<Car> cars = response.body();
                        DBManager.getInstance().put(DBKeys.CARS_LIST, cars);

                        if (cars != null && cars.size() > 0) {
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    if(DBManager.getInstance().exists(DBKeys.CURRENT_CAR)) {
                                        Intent intent = new Intent(mActivity, AutoActivity.class);
                                        mActivity.startActivity(intent);
                                        mActivity.finish();
                                    } else {
                                        Intent intent = new Intent(mActivity, MyCarsActivity.class);
                                        mActivity.startActivity(intent);
                                        mActivity.finish();
                                    }
                                }
                            }.sendEmptyMessageDelayed(0, 2500);
                        } else {
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    Intent intent = new Intent(mActivity, MyCarsActivity.class);
                                    mActivity.startActivity(intent);
                                    mActivity.finish();
                                }
                            }.sendEmptyMessageDelayed(0, 2500);
                        }
                    } else {
                        //Toast.makeText(getApplicationContext(), "response.isSuccessful() == false: " + response.message(), Toast.LENGTH_SHORT).show();
                        if (response.code() == 401) {
                            Intent intent = new Intent(mActivity, LoginActivity.class);
                            mActivity.startActivity(intent);
                            mActivity.finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "onFailure: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }.sendEmptyMessageDelayed(0, 3000);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
