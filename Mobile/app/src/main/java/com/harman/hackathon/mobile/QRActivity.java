package com.harman.hackathon.mobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.data.QrCode;
import com.harman.hackathon.mobile.db.DBKeys;
import com.harman.hackathon.mobile.db.DBManager;
import com.harman.hackathon.mobile.utils.ContextUtil;

import net.glxn.qrgen.android.QRCode;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QRActivity extends Activity {
    private static final String TAG = "QRActivity";

    private ImageView mIvQrCode;
    private Button mBtnQrCode;

    private Car mCurrentCar;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        mActivity = this;

        mIvQrCode = findViewById(R.id.ivQrCode);
        mBtnQrCode = findViewById(R.id.btnGetQR);

        if(DBManager.getInstance().exists(DBKeys.CURRENT_CAR)) {
            mCurrentCar = (Car) DBManager.getInstance().get(DBKeys.CURRENT_CAR, Car.class);
        } else {
            finish();
        }

        mBtnQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBtnQrCode.setEnabled(false);
                ContextUtil.getAutoApi().getQrCode(mCurrentCar.getCarID(),
                        ContextUtil.getAccessTokenDelegate().getAccessToken()).enqueue(new Callback<QrCode>() {
                    @Override
                    public void onResponse(Call<QrCode> call, Response<QrCode> response) {
                        if(response.isSuccessful()) {
                            QrCode qrCode = response.body();
                            Bitmap myBitmap = QRCode.from(qrCode.getQrCode()).bitmap();
                            mIvQrCode.setImageBitmap(myBitmap);
                            mBtnQrCode.setEnabled(false);
                        } else {
                            if (response.code() == 401) {
                                Intent intent = new Intent(mActivity, LoginActivity.class);
                                mActivity.startActivity(intent);
                                mActivity.finish();
                            }
                            if (response.code() == 400) {
                                Toast.makeText(QRActivity.this, response.message(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<QrCode> call, Throwable t) {
                        finish();
                    }
                });
            }
        });
    }
}
