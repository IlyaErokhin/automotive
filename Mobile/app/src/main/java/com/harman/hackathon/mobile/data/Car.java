package com.harman.hackathon.mobile.data;

import java.util.ArrayList;

public class Car {

    private String _id;
    private boolean started;
    private boolean opened;
    private String name;
    private String owner;

    public String getCarID() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getOwner() {
        return owner;
    }

    public void setStarted(boolean isStarted) {
        started = isStarted;
    }

    public boolean isStarted() {
        return started;
    }

    public void setOpened(boolean isOpened) {
        opened = isOpened;
    }

    public boolean isOpened() {
        return opened;
    }
}
