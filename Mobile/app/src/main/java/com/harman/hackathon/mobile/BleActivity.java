package com.harman.hackathon.mobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.harman.hackathon.mobile.data.Car;
import com.harman.hackathon.mobile.utils.AudioPlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BleActivity extends AppCompatActivity
        implements RecognitionListener, TextToSpeech.OnInitListener {
    private static String TAG = "BleActivity";

    private static final int REQUEST_ENABLE_BT = 2343;

    private BluetoothAdapter mBluetoothAdapter;

    private static String mDriver = "34:15:00:DE:7F:27";
    private static long mDriverLastConnection;
    private static boolean isWaitingClose = false;


    private Car car;

    private TextToSpeech mTTS;

    private SpeechRecognizer sr;

    private Button mBtnScanDevices;
    private Button mBtnStartSpeechRecognizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble);

        sr = SpeechRecognizer.createSpeechRecognizer(this);
        sr.setRecognitionListener(this);

        car = AutoActivity.getCarForTesting();

        mBtnScanDevices = (Button)findViewById(R.id.btnScanBle);
        mBtnStartSpeechRecognizer = (Button)findViewById(R.id.btnStartSpeechRecognizer);

        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        mTTS = new TextToSpeech(this, this);

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Use this check to determine whether BLE is supported on the device. Then
        // you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            //Toast.makeText(this, "ble_not_supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                //Finding devices
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    Integer rssi = new Short(intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, new Short("0"))).intValue();

                    long currentTime = new Date().getTime();
                    if (mDriverLastConnection == 0) {
                        mDriverLastConnection = currentTime;
                    }

                    if (device.getAddress().contentEquals(mDriver)) {
                        mBluetoothAdapter.cancelDiscovery();
                        mDriverLastConnection = currentTime;

                        if (!car.isOpened() && rssi > -70) {
                            car.setOpened(true);
                            new AudioPlayer().play(R.raw.open);
                            mTTS.speak("Юуууху! поехали", TextToSpeech.QUEUE_FLUSH, null);
                        }

                        if (car.isOpened() && !car.isStarted() && rssi < -81) {
                            if (isWaitingClose) {
                                isWaitingClose = false;
                                car.setOpened(false);
                                new AudioPlayer().play(R.raw.open);
                                mTTS.speak("Илья! не уходи!", TextToSpeech.QUEUE_FLUSH, null);
                            } else {
                                isWaitingClose = true;
                            }
                        } else {
                            isWaitingClose = false;
                        }

                    } else {
                        if (car.isOpened() && !car.isStarted() && (currentTime - mDriverLastConnection) > 10000) {
                            car.setOpened(false);
                            new AudioPlayer().play(R.raw.open);
                            mTTS.speak("Илья! не уходи далеко!", TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                    // Add the name and address to an array adapter to show in a ListView
                    Log.d(TAG, device.getAddress() + " " + (device.getName() != null ? device.getName() : "") + " rssi = " + rssi);
                }  else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    Log.v(TAG,"Entered the Finished ");
                    mBluetoothAdapter.startDiscovery();
                }
            }
        };

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        mBtnScanDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        mBluetoothAdapter.startDiscovery();

                        sendEmptyMessageDelayed(0, 100);
                    }
                }.sendEmptyMessage(0);
            }
        });

        mBtnStartSpeechRecognizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"com.harman.hackathon.mobile");

                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,5);
                sr.startListening(intent);
            }
        });
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {
        Log.d(TAG, "onReadyForSpeech");
    }

    @Override
    public void onBeginningOfSpeech()
    {
        Log.d(TAG, "onBeginningOfSpeech");
        //Toast.makeText(getApplicationContext(), "Beginning Of Speech", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {
        Log.d(TAG, "onRmsChanged");
    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {
        Log.d(TAG, "onBufferReceived");
    }

    @Override
    public void onEndOfSpeech()
    {
        Log.d(TAG, "onEndofSpeech");
        //Toast.makeText(getApplicationContext(), "End Of Speech", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int error)
    {
        Log.d(TAG,  "error " +  error);
    }

    @Override
    public void onResults(Bundle results)
    {
        String str = new String();
        Log.d(TAG, "onResults " + results);
        ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        for (int i = 0; i < data.size(); i++)
        {
            Log.d(TAG, "result " + data.get(i));
            str += data.get(i);
        }
        Log.d(TAG, "results: "+String.valueOf(data.size()));
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {
        Log.d(TAG, "onPartialResults");
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {
        Log.d(TAG, "onEvent " + eventType);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            Locale locale = new Locale("ru");

            int result = mTTS.setLanguage(locale);
            //int result = mTTS.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Извините, этот язык не поддерживается");
            }

        } else {
            Log.e("TTS", "Ошибка!");
        }
    }
}
