package com.harman.hackathon.auto;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.harman.hackathon.auto.data.Car;
import com.harman.hackathon.auto.data.CurrentCar;
import com.harman.hackathon.auto.data.User;
import com.harman.hackathon.auto.utils.AudioPlayer;
import com.harman.hackathon.auto.utils.ContextUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends Activity
        implements TextToSpeech.OnInitListener, RecognitionListener {
    private static final String TAG = "MainActivity";

    private Activity mActivity;

    private ImageView mBtnEngine;
    private ImageView mBtnLock;

    private HashMap<String, User> mMyUsers;
    private User mCurrentUser;

    private EditText mEtSeats;
    private EditText mEtMusic;
    private EditText mEtPower;
    private EditText mEtMirrors;

    private Button mBtnUpdate;

    private TextView mTvName;

    private static final int REQUEST_ENABLE_BT = 2343;

    private TextToSpeech mTTS;
    private SpeechRecognizer sr;

    private BluetoothAdapter mBluetoothAdapter;

    private static long mDriverLastConnection;
    private static boolean isWaitingClose = false;

    private static Handler handler = null;

    private long currentTime;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;

        mBtnEngine = (ImageView) findViewById(R.id.btnEngine);
        mBtnLock = (ImageView) findViewById(R.id.btnLock);

        mEtSeats = (EditText) findViewById(R.id.etSeats);
        mEtMusic = (EditText) findViewById(R.id.etMusic);
        mEtPower = (EditText) findViewById(R.id.etPower);
        mEtMirrors = (EditText) findViewById(R.id.etMirrors);

        mTvName = (TextView) findViewById(R.id.tvName);

        mBtnUpdate = (Button) findViewById(R.id.btnUpdateData);

        mBtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateValues();
            }
        });

        mTTS = new TextToSpeech(this, this);

        sr = SpeechRecognizer.createSpeechRecognizer(this);
        sr.setRecognitionListener(this);

        ContextUtil.getAutoApi().getAllCars().enqueue(new Callback<ArrayList<Car>>() {
            @Override
            public void onResponse(Call<ArrayList<Car>> call, Response<ArrayList<Car>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Car> cars = response.body();

                    for (Car car : cars) {
                        if (car.getCarID().contentEquals(getAndroidID())) {
                            CurrentCar.getInstance().updateCar(car);
                            mMyUsers = car.getUsers();

                            mBtnEngine.setImageDrawable(ContextCompat.getDrawable(mActivity,
                                    CurrentCar.getInstance().isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));

                            if (CurrentCar.getInstance().isStarted()) new AudioPlayer().engineStart();

                            mBtnLock.setImageDrawable(ContextCompat.getDrawable(mActivity,
                                    CurrentCar.getInstance().isOpened() ? R.drawable.unlocked : R.drawable.locked));

                            break;
                        }
                    }

                    if (!CurrentCar.isInst()) {
                        Car newCar = initCar();
                        ContextUtil.getAutoApi().addCar(initCar()).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.d(TAG, Boolean.toString(response.isSuccessful()));
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d(TAG, t.getMessage());
                            }
                        });
                        CurrentCar.getInstance().updateCar(newCar);
                        mMyUsers = newCar.getUsers();

                        mBtnEngine.setImageDrawable(ContextCompat.getDrawable(mActivity,
                                CurrentCar.getInstance().isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));

                        mBtnLock.setImageDrawable(ContextCompat.getDrawable(mActivity,
                                CurrentCar.getInstance().isOpened() ? R.drawable.unlocked : R.drawable.locked));
                    }
                } else {
                    Log.d(TAG, response.message());
                }

                if(CurrentCar.isInst()) {
                    updateCar();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Car>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });

        mBtnEngine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CurrentCar.getInstance().isStarted()) {
                    new AudioPlayer().engineStart();
                } else {
                    new AudioPlayer().engineStop();
                }
                CurrentCar.getInstance().setStarted(!CurrentCar.getInstance().isStarted());
//                mBtnEngine.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
//                        CurrentCar.getInstance().isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));
                handler.removeMessages(0);
                uploadCar();

            }
        });

        mBtnLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new AudioPlayer().open();
                CurrentCar.getInstance().setOpened(!CurrentCar.getInstance().isOpened());
//                mBtnLock.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
//                        CurrentCar.getInstance().isOpened() ? R.drawable.unlocked : R.drawable.locked));
                handler.removeMessages(0);
                uploadCar();

            }
        });

        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Use this check to determine whether BLE is supported on the device. Then
        // you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            //Toast.makeText(this, "ble_not_supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (mMyUsers == null) return;

                //Finding devices
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    Integer rssi = new Short(intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, new Short("0"))).intValue();

                    ArrayList<String> allAddresses = new ArrayList<>();

                    for (User user : mMyUsers.values()) {
                        allAddresses.add(user.getBluetooth());
                    }

                    if (allAddresses.contains(device.getAddress())) {
                        User currentUser = null;

                        for (User user : mMyUsers.values()) {
                            if(device.getAddress().contentEquals(user.getBluetooth())) {
                                currentUser = user;
                            }
                        }

                        currentTime = new Date().getTime();
                        if (mDriverLastConnection == 0) {
                            mDriverLastConnection = currentTime;
                        }

                        mBluetoothAdapter.cancelDiscovery();
                        mDriverLastConnection = currentTime;

                        if (!CurrentCar.getInstance().isOpened() && rssi > -70) {
                            if (mCurrentUser != null) return;
                            CurrentCar.getInstance().setOpened(true);
                            new AudioPlayer().open();
                            speak("Привет " + currentUser.getName() + "! Автомобиль открыт!");
                            mCurrentUser = currentUser;
                            showData();
                            uploadCar();
                        }

                        if (CurrentCar.getInstance().isOpened() && !CurrentCar.getInstance().isStarted() && rssi < -83) {
                            if (isWaitingClose) {
                                isWaitingClose = false;
                                CurrentCar.getInstance().setOpened(false);
                                new AudioPlayer().open();
                                speak("До свидания " + currentUser.getName() + "! Автомобиль закрыт!");
                                hideData();
                                uploadCar();
                            } else {
                                isWaitingClose = true;
                            }
                        } else {
                            isWaitingClose = false;
                        }

                    }
                    // Add the name and address to an array adapter to show in a ListView
                    Log.d(TAG, device.getAddress() + " " + (device.getName() != null ? device.getName() : "") + " rssi = " + rssi);
                }  else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    Log.v(TAG,"Entered the Finished ");
                    mBluetoothAdapter.startDiscovery();
                }
            }
        };

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(!mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.startDiscovery();
                } else {
                    if (CurrentCar.getInstance().isOpened() && !CurrentCar.getInstance().isStarted()
                            && (new Date().getTime() - mDriverLastConnection) > 10000) {
                        if (mCurrentUser != null) {
                            CurrentCar.getInstance().setOpened(false);
                            new AudioPlayer().open();
                            mBluetoothAdapter.cancelDiscovery();
                            speak("Автомобиль закрыт!");
                            hideData();
                            uploadCar();
                        }
                    }
                }

                sendEmptyMessageDelayed(0, 1000);
            }
        };

        handler.sendEmptyMessageDelayed(0,1000);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            Locale locale = new Locale("ru");

            int result = mTTS.setLanguage(locale);
            //int result = mTTS.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Извините, этот язык не поддерживается");
            } else {
                //mTTS.speak("Hello" + mName + "! Where are we going?", TextToSpeech.QUEUE_FLUSH, null);
            }

            //speak("Я включилась");

        } else {
            Log.e("TTS", "Ошибка!");
        }
    }

    private void speak(String text) {
        if (mTTS != null) {
            try {
                mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {
        Log.d(TAG, "onReadyForSpeech");
    }

    @Override
    public void onBeginningOfSpeech()
    {
        Log.d(TAG, "onBeginningOfSpeech");
        //Toast.makeText(getApplicationContext(), "Beginning Of Speech", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {
        Log.d(TAG, "onRmsChanged");
    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {
        Log.d(TAG, "onBufferReceived");
    }

    @Override
    public void onEndOfSpeech()
    {
        Log.d(TAG, "onEndofSpeech");
        //Toast.makeText(getApplicationContext(), "End Of Speech", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int error)
    {
        Log.d(TAG,  "error " +  error);
    }

    @Override
    public void onResults(Bundle results)
    {
        String str = new String();
        Log.d(TAG, "onResults " + results);
        ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        for (int i = 0; i < data.size(); i++)
        {
            Log.d(TAG, "result " + data.get(i));
            str += data.get(i);
        }
        Log.d(TAG, "results: "+String.valueOf(data.size()));
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {
        Log.d(TAG, "onPartialResults");
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {
        Log.d(TAG, "onEvent " + eventType);
    }

    private Car initCar() {
        Car car = new Car();

        car.setName("Harman Car");
        car.setUsers(new HashMap<String, User>());
        car.setOpened(false);
        car.setStarted(false);
        car.setCarID(getAndroidID());
        car.setPassword(getPassword());
        car.setOwner("");

        return car;
    }

    private String getAndroidID() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID).toUpperCase();
    }

    private String getPassword() {
        return "1010101010";
    }

    private void uploadCar() {
        Car car = CurrentCar.getInstance().getCar();
        ContextUtil.getAutoApi().updateCar(car).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, Boolean.toString(response.isSuccessful()));
                handler.sendEmptyMessageDelayed(0, 1000);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                handler.sendEmptyMessageDelayed(0, 1000);
            }
        });
    }

    private void updateCar() {
        ContextUtil.getAutoApi().getCar(CurrentCar.getInstance().getCar().getCarID(),
                CurrentCar.getInstance().getCar().getPassword()).enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if (response.isSuccessful()) {
                    Car car = response.body();

                    if(CurrentCar.getInstance().isOpened() != car.isOpened()) {
                        mBtnLock.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                car.isOpened() ? R.drawable.unlocked : R.drawable.locked));
                        new AudioPlayer().open();
                    }

                    if(CurrentCar.getInstance().isStarted() != car.isStarted()) {
                        mBtnEngine.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                car.isStarted() ? R.drawable.engine_stop_button : R.drawable.engine_start_button));

                        if(car.isStarted()) {
                            new AudioPlayer().engineStart();
                        } else {
                            new AudioPlayer().engineStop();
                        }
                    }
                    CurrentCar.getInstance().updateCar(car);
                    mMyUsers = car.getUsers();
                }
                updateCar();
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                updateCar();
            }
        });
    }

    private void showData() {
        if (mCurrentUser != null) {
            mTvName.setText("Hello " + mCurrentUser.getName() == null ? "null" : mCurrentUser.getName());
            mEtSeats.setEnabled(true);
            mEtSeats.setText(mCurrentUser.getSeats());
            mEtMusic.setEnabled(true);
            mEtMusic.setText(mCurrentUser.getMusic());
            mEtPower.setEnabled(true);
            mEtPower.setText(mCurrentUser.getPower());
            mEtMirrors.setEnabled(true);
            mEtMirrors.setText(mCurrentUser.getMirrors());
        } else {
            mTvName.setText("");
            mEtSeats.setEnabled(false);
            mEtSeats.setText("");
            mEtMusic.setEnabled(false);
            mEtMusic.setText("");
            mEtPower.setEnabled(false);
            mEtPower.setText("");
            mEtMirrors.setEnabled(false);
            mEtMirrors.setText("");
        }
    }

    private void hideData() {
        mCurrentUser = null;
        mTvName.setText("");
        mEtSeats.setEnabled(false);
        mEtSeats.setText("");
        mEtMusic.setEnabled(false);
        mEtMusic.setText("");
        mEtPower.setEnabled(false);
        mEtPower.setText("");
        mEtMirrors.setEnabled(false);
        mEtMirrors.setText("");
    }

    private void updateValues() {
        if (mCurrentUser != null) {
            mCurrentUser.setSeats(mEtSeats.getText().toString());
            mCurrentUser.setMusic(mEtMusic.getText().toString());
            mCurrentUser.setPower(mEtPower.getText().toString());
            mCurrentUser.setMirrors(mEtMirrors.getText().toString());

            for (HashMap.Entry<String, User> user : mMyUsers.entrySet()) {
                if (user.getValue().getBluetooth().contentEquals(mCurrentUser.getBluetooth())) {
                    mMyUsers.put(user.getKey(), mCurrentUser);
                    CurrentCar.getInstance().setUsers(mMyUsers);
//                    uploadCar();
                    user.setValue(mCurrentUser);
                    ContextUtil.getAutoApi().updateUser(getAndroidID(), user.getKey(), user.getValue()).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Log.d(TAG, Boolean.toString(response.isSuccessful()));
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d(TAG, t.getMessage());
                        }
                    });
                    break;
                }
            }
        }
    }
}
