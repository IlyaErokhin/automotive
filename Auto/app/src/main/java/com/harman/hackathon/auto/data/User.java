package com.harman.hackathon.auto.data;

public class User {

    private String name;
    private String bluetooth;
    private String seats;
    private String music;
    private String power;
    private String mirrors;

    public String getName() {
        return name;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getMirrors() {
        return mirrors;
    }

    public void setMirrors(String mirrors) {
        this.mirrors = mirrors;
    }
}
