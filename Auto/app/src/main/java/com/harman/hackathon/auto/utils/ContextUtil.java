package com.harman.hackathon.auto.utils;

import android.content.Context;

import com.harman.hackathon.auto.AutoApi;
import com.harman.hackathon.auto.AutoApiDelegate;

public class ContextUtil {

    private static Context appContext;

    public static void setAppContext(Context context) {
        if (appContext == null) {
            appContext = context;
        }
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static AutoApi getAutoApi() {
        return ((AutoApiDelegate) getAppContext()).getApi();
    }
}
