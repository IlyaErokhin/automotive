package com.harman.hackathon.auto;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.harman.hackathon.auto.utils.ContextUtil;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AutoAplication extends Application
        implements AutoApiDelegate {

    private static AutoApi autoApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        ContextUtil.setAppContext(this);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://ec2-18-195-187-89.eu-central-1.compute.amazonaws.com:8877/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        autoApi = retrofit.create(AutoApi.class);
    }

    @Override
    public AutoApi getApi() {
        return autoApi;
    }
}
