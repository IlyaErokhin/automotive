package com.harman.hackathon.auto;

import com.harman.hackathon.auto.data.Car;
import com.harman.hackathon.auto.data.User;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AutoApi {

    @POST("/")
    Call<ResponseBody> updateCar(
            @Body Car car
    );

    @GET("/{car_id}")
    Call<Car> getCar(
            @Path("car_id") String carID,
            @Query("password") String password
    );

    @PUT("/upload/car")
    Call<ResponseBody> addCar(
            @Body Car car
    );

    @GET("/car")
    Call<ArrayList<Car>> getAllCars();

    @POST("/{car_id}/change/user/{user_id}")
    Call<ResponseBody> updateUser(
            @Path("car_id") String carID,
            @Path("user_id") String userID,
            @Body User user
    );
}
