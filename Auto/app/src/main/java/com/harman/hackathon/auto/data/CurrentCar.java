package com.harman.hackathon.auto.data;

import java.util.HashMap;

public class CurrentCar {

    private static CurrentCar instance;

    private Car mCurrentCar;

    public static synchronized CurrentCar getInstance() {
        if (instance == null) {
            instance = new CurrentCar();
        }
        return instance;
    }

    public static synchronized boolean isInst(){
        if (instance == null || instance.mCurrentCar == null){
            return false;
        } else {
            return true;
        }
    }


    public void updateCar(Car newCar) {
        mCurrentCar = newCar;
    }

    public Car getCar() {
        return  mCurrentCar;
    }


    public void setStarted(boolean isStarted) {
        if(mCurrentCar != null) {
            mCurrentCar.setStarted(isStarted);
        }
    }

    public boolean isStarted() {
        if(mCurrentCar != null) {
            return mCurrentCar.isStarted();
        }
        return false;
    }

    public void setOpened(boolean isOpened) {
        if(mCurrentCar != null) {
            mCurrentCar.setOpened(isOpened);
        }
    }

    public boolean isOpened() {
        if(mCurrentCar != null) {
            return mCurrentCar.isOpened();
        }
        return false;
    }

    public void setUsers(HashMap<String, User> users) {
        mCurrentCar.setUsers(users);
    }

    public HashMap<String, User> getUsers() {
        return mCurrentCar.getUsers();
    }
}
