package com.harman.hackathon.auto;

public interface AutoApiDelegate {
    AutoApi getApi();
}
