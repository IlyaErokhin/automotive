package com.harman.hackathon.auto.data;

import java.util.HashMap;

public class Car {

    private String _id;
    private String password;
    private boolean started;
    private boolean opened;
    private String name;
    private String owner;
    private HashMap<String,User> users;

    public String getCarID() {
        return _id;
    }

    public void setCarID(String vim) {
        _id = vim;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setStarted(boolean isStarted) {
        started = isStarted;
    }

    public boolean isStarted() {
        return started;
    }

    public void setOpened(boolean isOpened) {
        opened = isOpened;
    }

    public boolean isOpened() {
        return opened;
    }

    public HashMap<String, User> getUsers() {
        return users;
    }

    public void setUsers(HashMap<String, User> users) {
        this.users = users;
    }
}

