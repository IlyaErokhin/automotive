package com.harman.hackathon.auto.utils;

import android.media.MediaPlayer;

import com.harman.hackathon.auto.R;

public class AudioPlayer {

    private static MediaPlayer mOpenPlayer;
    private static MediaPlayer mEngineStart;
    private static MediaPlayer mEngine;
    private static MediaPlayer mEngineStop;

    private void stopOpen() {
        if (mOpenPlayer != null) {
            mOpenPlayer.release();
            mOpenPlayer = null;
        }
    }

    private void stopEngineStart() {
        if (mEngineStart != null) {
            mEngineStart.release();
            mEngineStart = null;
        }
    }

    private void stopEngine() {
        if (mEngine != null) {
            mEngine.release();
            mEngine = null;
        }
    }

    private void stopEngineStop() {
        if (mEngineStop != null) {
            mEngineStop.release();
            mEngineStop = null;
        }
    }

    public void open() {
        mOpenPlayer = MediaPlayer.create(ContextUtil.getAppContext(), R.raw.open);
        mOpenPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stopOpen();
            }
        });

        mOpenPlayer.start();
    }

    public void engineStart() {
        mEngineStart = MediaPlayer.create(ContextUtil.getAppContext(), R.raw.engine_start);
        mEngineStart.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stopEngineStart();
                mEngine = MediaPlayer.create(ContextUtil.getAppContext(), R.raw.engine);
                mEngine.setLooping(true);
                mEngine.start();
            }
        });

        mEngineStart.start();
    }

    public void engineStop() {
        mEngineStop = MediaPlayer.create(ContextUtil.getAppContext(), R.raw.engine_stop);
        mEngineStop.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stopEngineStop();
            }
        });

        stopEngineStart();
        stopEngine();
        mEngineStop.start();
    }
}
